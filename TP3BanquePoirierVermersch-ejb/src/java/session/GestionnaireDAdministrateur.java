/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Administrateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Manon
 */
@Stateless
@LocalBean
public class GestionnaireDAdministrateur {

    @PersistenceContext(unitName = "TP3BanquePoirierVermersch-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void creerAdministrateur(Administrateur a){
        em.persist(a);
    }
    
    public List<Administrateur> getAllAdministrateurs(){
        Query query = em.createNamedQuery("Administrateur.findAll");
        return query.getResultList();
    }
    
    public List<Administrateur> getAdministrateurs(int start, int nbAdministrateurs){
        List<Administrateur> listAdministrateurs = getAllAdministrateurs();
        if (start + nbAdministrateurs > listAdministrateurs.size() - 1){
            return listAdministrateurs.subList(start, listAdministrateurs.size() - 1);
        }
        return listAdministrateurs.subList(start, start + nbAdministrateurs);
    }
    
    public Administrateur getAdministrateur(int numeroAdministrateur){
        Query q = em.createNamedQuery("Administrateur.findById");
        q.setParameter("id", Long.parseLong(""+numeroAdministrateur));
        return (Administrateur) q.getSingleResult();
    }
    
    public Administrateur updateAdministrateur(Administrateur a){
        return em.merge(a);
    }
    
    public void creerAdministrateursTest(){
        Administrateur a = new Administrateur("AdminNom","AdminPrenom","Nice","admin","password", 1000);
        creerAdministrateur(a);
    }
}
