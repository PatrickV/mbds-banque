/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.CompteBancaire;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Manon
 */
@Stateless
@LocalBean
public class GestionnaireDeCompteBancaire {

    @PersistenceContext(unitName = "TP3BanquePoirierVermersch-ejbPU")
    private EntityManager em;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void creerCompte(CompteBancaire c){
        em.persist(c);
    }

    public List<CompteBancaire> getAllComptes() {
       Query query = em.createNamedQuery("CompteBancaire.findAll");
       return query.getResultList();
    }
    
    public List<CompteBancaire> getComptes(int start, int nbComptes) {
        List<CompteBancaire> listComptes = getAllComptes();
        if (start + nbComptes > listComptes.size() - 1) {
            return listComptes.subList(start, listComptes.size() - 1);
        }
       return listComptes.subList(start, start + nbComptes);
    }
    
    public CompteBancaire getCompte(int idCompte) {
        Query q = em.createNamedQuery("CompteBancaire.findByCompteBancaireId");
        q.setParameter("id", Long.parseLong(idCompte + ""));
        
        return (CompteBancaire) q.getSingleResult();
    }
    
    public CompteBancaire updateCompte(CompteBancaire c){
        return em.merge(c);
    }
    
    public void creerComptesTest(){
        creerCompte(new CompteBancaire("John Lennon", 150000));
        creerCompte(new CompteBancaire("Paul McCartney", 950000));
        creerCompte(new CompteBancaire("Ringo Starr", 20000));
        creerCompte(new CompteBancaire("Georges Harrisson", 100000));
    }
    

    public void persist(Object object) {
        em.persist(object);
    }
    
    public void transferer(int idCompteSource, int idCompteDestination, int montant) {
        CompteBancaire cSource = getCompte(idCompteSource);
        CompteBancaire cDestination = getCompte(idCompteDestination);
        
        cSource.retirer(montant);
        cDestination.deposer(montant);
        
        updateCompte(cSource);
        updateCompte(cDestination);
    }
    
    public void deposer (int idCompte, int montant) {
        CompteBancaire c = getCompte(idCompte);
        
        c.deposer(montant);
        
        updateCompte(c);
    }
    
    public int retirer (int idCompte, int montant) {
        CompteBancaire c = getCompte(idCompte);
        
        int somme = c.retirer(montant);
        
        updateCompte(c);
        
        return somme;
    }
    
    public void supprimer (int idCompte) {
        Query q = em.createNamedQuery("CompteBancaire.DeleteByCompteBancaireId");
        q.setParameter("id", "" + idCompte);
        q.executeUpdate();
    }
    
    public CompteBancaire getCompteBancaire(int idCompte){
        return em.find(CompteBancaire.class, idCompte);
    }
    
    
}
