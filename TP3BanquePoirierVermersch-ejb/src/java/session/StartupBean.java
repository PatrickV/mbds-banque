/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Client;
import entities.Conseiller;
import entities.Personne;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author patrick
 */
@Singleton
@LocalBean
@Startup
public class StartupBean {

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB
    private GestionnaireDePersonne gestionnaireDePersonne;
    @EJB
    private GestionnaireDAdministrateur gestionnaireDAdministrateur;
    @EJB
    private GestionnaireDeClient gestionnaireDeClient;
    @EJB
    private GestionnaireDeConseiller gestionnaireDeConseiller;

    
    
    @PostConstruct
    public void start () {
        gestionnaireDeCompteBancaire.creerComptesTest();
        gestionnaireDePersonne.creerPersonnesTest();
        gestionnaireDAdministrateur.creerAdministrateursTest();
        
        List<Conseiller> lesConseillers = gestionnaireDeConseiller.creerConseillersTest();
        List<Client> lesClients = gestionnaireDeClient.creerClientsTest(lesConseillers);
        
        lesConseillers.get(0).ajouterUnClient(lesClients.get(0));
        lesConseillers.get(1).ajouterUnClient(lesClients.get(1));
        lesConseillers.get(2).ajouterUnClient(lesClients.get(2));
        lesConseillers.get(2).ajouterUnClient(lesClients.get(3));
        
        gestionnaireDeConseiller.updateConseiller(lesConseillers.get(0));
        gestionnaireDeConseiller.updateConseiller(lesConseillers.get(1));
        gestionnaireDeConseiller.updateConseiller(lesConseillers.get(2));
        
    }
}
