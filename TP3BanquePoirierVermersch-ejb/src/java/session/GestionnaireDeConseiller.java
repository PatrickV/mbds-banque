/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Conseiller;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Manon
 */
@Stateless
@LocalBean
public class GestionnaireDeConseiller {

    @PersistenceContext(unitName = "TP3BanquePoirierVermersch-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public void creerConseiller(Conseiller c){
        em.persist(c);
    }
    
    public List<Conseiller> getAllConseillers() {
        Query query = em.createNamedQuery("Conseiller.findAll");
        return query.getResultList();
    }
    
    public List<Conseiller> getConseillers(int start, int nbConseillers){
        List<Conseiller> listConseillers = getAllConseillers();
        if (start + nbConseillers > listConseillers.size() - 1){
            return listConseillers.subList(start, listConseillers.size() - 1);
        }
        return listConseillers.subList(start, start + nbConseillers);
    }
    
    public Conseiller getConseiller(int idConseiller){
        Query q = em.createNamedQuery("Conseiller.findById");
        q.setParameter("id", Long.parseLong(idConseiller + ""));
        return (Conseiller) q.getSingleResult();
    }
    
    public Conseiller updateConseiller(Conseiller c){
        return em.merge(c);
    }
    
    public List<Conseiller> creerConseillersTest(){
        
        List<Conseiller> mesConseillers = new ArrayList<>();
        Conseiller c1 = new Conseiller("ConseillerNom","ConseilerPrenom","Nice","conseiller","password", 1000);
        creerConseiller(c1);
        Conseiller c2 = new Conseiller("NomConseiller","Conseiller","Paris","Conseillerlogin","password", 1000);
        creerConseiller(c2);
        Conseiller c3 = new Conseiller("Conseiller","PrenomConseiller","Lyon","loginConseiller","password", 1000);
        creerConseiller(c3);
        mesConseillers.add(c1);
        mesConseillers.add(c2);
        mesConseillers.add(c3);
        return mesConseillers;
    }
}
