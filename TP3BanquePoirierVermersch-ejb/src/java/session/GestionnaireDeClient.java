/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Client;
import entities.Conseiller;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Manon
 */
@Stateless
@LocalBean
public class GestionnaireDeClient {
    
    @PersistenceContext(unitName = "TP3BanquePoirierVermersch-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public void persist1(Object object) {
        em.persist(object);
    }
    
    public void creerClient(Client c){
        em.persist(c);
    }
    
    public List<Client> getAllClients(){
        Query query = em.createNamedQuery("Client.findAll");
        return query.getResultList();
    }
    
    public List<Client> getClients(int start, int nbClients){
        List<Client> listClients = getAllClients();
        if (start + nbClients > listClients.size() - 1){
            return listClients.subList(start, listClients.size() - 1);
        }
        return listClients.subList(start, start + nbClients);
    }
    public Client getClient(int numerodeclient){
        Query q = em.createNamedQuery("Client.findByNumeroDeClient");
        q.setParameter("id", Long.parseLong("" + numerodeclient));
        return (Client) q.getSingleResult();
    }
    
    public Client updateClient(Client c){
        return em.merge(c);
    }
    
    
    public List<Client> creerClientsTest(List<Conseiller> lesConseillers){
        List<Client> lesClients = new ArrayList<>();
        
        Client c1 = new Client("Client 1", "Client 1", "Adresse 1", "client", "password", 12, lesConseillers.get(0));
        Client c2 = new Client("Client 2", "Client 2", "Adresse 2", "Login2", "password", 23, lesConseillers.get(1));
        Client c3 = new Client("Client 3", "Client 3", "Adresse 3", "Login3", "password", 34, lesConseillers.get(2));
        Client c4 = new Client("Client 4", "Client 4", "Adresse 4", "Login4", "password", 45, lesConseillers.get(2));
        
        creerClient(c1);
        creerClient(c2);
        creerClient(c3);
        creerClient(c4);
        
        lesClients.add(c1);
        lesClients.add(c2);
        lesClients.add(c3);
        lesClients.add(c4);
        
        return lesClients;
    }
    
}
