/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entities.Personne;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Manon
 */
@Stateless
@LocalBean
public class GestionnaireDePersonne {

    @PersistenceContext(unitName = "TP3BanquePoirierVermersch-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public void creerPersonne(Personne p){
        em.persist(p);
    }
    
    public List<Personne> getAllPersonnes(){
        Query query = em.createNamedQuery("Personne.findAll");
        return query.getResultList();
    }
    
    public List<Personne> getPersonnes(int start, int nbPersonnes){
        List<Personne> listPersonnes = getAllPersonnes();
        if(start + nbPersonnes > listPersonnes.size() - 1){
            return listPersonnes.subList(start, listPersonnes.size() - 1);
        }
        return listPersonnes.subList(start, start + nbPersonnes);
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public Personne getPersonne(int idPersonne){
        Query q = em.createNamedQuery("Personne.findByPersonneId");
        q.setParameter("id", Long.parseLong(idPersonne + ""));
        return (Personne) q.getSingleResult();
    }
    
    public Personne getPersonneUsername(String usernamePersonne){
        Query q = em.createNamedQuery("Personne.findByPersonneUsername");
        q.setParameter("username", ""+usernamePersonne);
        try{
            Personne p = (Personne) q.getSingleResult();
            return p;
        } catch (NoResultException e) {
            return null;
        } 
    }
    
    public Personne updatePersonne(Personne p){
        return em.merge(p);
    }
    
    public void creerPersonnesTest(){
        creerPersonne(new Personne("Lennon","John","Nice","admin1","password"));
        creerPersonne(new Personne("McCartney", "Paul", "Antibes", "Conseiller1", "password"));
        creerPersonne(new Personne("Starr","Ringo","St-Vallier", "Client1", "password"));
        creerPersonne(new Personne("Toto","Ringo","St-Vallier", "login", "password"));
    }
    
    public void supprimer(int idPersonne){
        Query q = em.createNamedQuery("Personne.DeleteByPersonneId");
        q.setParameter("id", "" + idPersonne);
        q.executeUpdate();
    }
}
