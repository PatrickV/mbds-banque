/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author patrick
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findById", query = "SELECT c FROM Client c WHERE c.id=:id"),
    @NamedQuery(name = "Client.findByNumeroDeClient", query = "SELECT c FROM Client c WHERE c.id=:id")
})
@DiscriminatorValue(value = "client")
public class Client extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private List<Integer> numerosDeComptes;
    private int numeroDeClient;
    private Conseiller conseiller;
    
    public Client () {
        super();
    }
    
    public Client(String nom, String prenom, String adresse, String login, String password, int numeroDeClient, Conseiller conseiller) {
        super(nom, prenom, adresse, login, password, "client");
        this.numeroDeClient = numeroDeClient;
        this.conseiller = conseiller;
    }

    /**
     * @return the numerosDeComptes
     */
    public List<Integer> getNumerosDeComptes() {
        return numerosDeComptes;
    }

    /**
     * @param numerosDeComptes the numerosDeComptes to set
     */
    public void setNumerosDeComptes(List<Integer> numerosDeComptes) {
        this.numerosDeComptes = numerosDeComptes;
    }

    /**
     * @return the numeroDeClient
     */
    public int getNumeroDeClient() {
        return numeroDeClient;
    }

    /**
     * @param numeroDeClient the numeroDeClient to set
     */
    public void setNumeroDeClient(int numeroDeClient) {
        this.numeroDeClient = numeroDeClient;
    }

    /**
     * @return the conseiller
     */
    public Conseiller getConseiller() {
        return conseiller;
    }

    /**
     * @param conseiller the conseiller to set
     */
    public void setConseiller(Conseiller conseiller) {
        this.conseiller = conseiller;
    }
    
}
