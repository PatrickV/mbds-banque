/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author patrick
 */
@Entity
@NamedQueries({
    @NamedQuery (name = "Administrateur.findAll", query = "SELECT a FROM Administrateur a"),
    @NamedQuery (name = "Administrateur.findId", query = "SELECT a FROM Administrateur a WHERE a.id = :id"),
    @NamedQuery (name = "Administrateur.findByNumeroAdministrateur", query = "SELECT a FROM Administrateur a WHERE a.numeroAdministrateur = :numeroadministrateur")
})
@DiscriminatorValue(value = "administrateur")
public class Administrateur extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    private int numeroAdministrateur;
    
    public Administrateur () {
        super();
    }

    public Administrateur (String nom, String prenom, String adresse, String login, String password, int numeroAdministrateur) {
        super(nom, prenom, adresse, login, password, "administrateur");
        this.numeroAdministrateur = numeroAdministrateur;
    }

    public int getNumeroAdministrateur() {
        return numeroAdministrateur;
    }

    public void setNumeroAdministrateur(int numeroAdministrateur) {
        this.numeroAdministrateur = numeroAdministrateur;
    }
    
}
