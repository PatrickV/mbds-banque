/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author patrick
 */
@Entity
@NamedQueries({
    @NamedQuery (name = "Personne.findAll", query = "SELECT p FROM Personne p"),
    @NamedQuery (name = "Personne.findByPersonneId", query  = "SELECT p FROM Personne p WHERE p.id = :id"),
    @NamedQuery (name = "Personne.findByPersonneUsername", query  = "SELECT p FROM Personne p WHERE p.username = :username"),
    @NamedQuery (name = "Personne.DeleteByPersonneId", query = "DELETE FROM Personne p WHERE p.id = :id")
})
@DiscriminatorColumn(name="discriminant", discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue(value = "personne")
public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name="discriminant", insertable = false, updatable = false)
    private String discriminant;
    
    private String nom;
    private String prenom;
    private String adresse;
    private String username;
    private String password;
    
    public Personne () {
        
    }
    
   /* public Personne (String nom, String prenom, String adresse) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
    }*/
    
    public Personne (String nom, String prenom, String adresse, String username, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.username = username;
        this.password = BCrypt.hashpw(password, BCrypt.gensalt(5));
        this.discriminant = "personne";
    }
    
    public Personne (String nom, String prenom, String adresse, String username, String password, String discriminant) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.username = username;
        this.password = BCrypt.hashpw(password, BCrypt.gensalt(5));
        this.discriminant = discriminant;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Personne[ id=" + id + " ]";
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = BCrypt.hashpw(password, BCrypt.gensalt(5));
    }

    /**
     * @return the discriminant
     */
    public String getDiscriminant() {
        return discriminant;
    }

    /**
     * @param discriminant the discriminant to set
     */
    public void setDiscriminant(String discriminant) {
        this.discriminant = discriminant;
    }
}
