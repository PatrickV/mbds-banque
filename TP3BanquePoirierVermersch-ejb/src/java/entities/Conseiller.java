/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author patrick
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Conseiller.findAll", query = "SELECT c FROM Conseiller c"),
    @NamedQuery(name = "Conseiller.findById", query = "SELECT c FROM Conseiller c WHERE c.id = :id")
})
@DiscriminatorValue(value = "conseiller")
public class Conseiller extends Personne implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int numeroEmploye;
    private List<Client> listeDesClients;
    
    public Conseiller () {
        super();
        this.listeDesClients = new ArrayList<>();
    }

    public Conseiller(String nom, String prenom, String adresse, String login, String password, int numeroEmploye) {
        super(nom, prenom, adresse, login, password, "conseiller");
        this.numeroEmploye = numeroEmploye;
        this.listeDesClients = new ArrayList<>();
    }
    
    public void ajouterUnClient(Client client) {
        this.listeDesClients.add(client);
    }

    public int getNumeroEmploye() {
        return numeroEmploye;
    }

    public void setNumeroEmploye(int numeroEmploye) {
        this.numeroEmploye = numeroEmploye;
    }

    public List<Client> getListeDesClients() {
        return listeDesClients;
    }

    public void setListeDesClients(List<Client> listeDesClients) {
        this.listeDesClients = listeDesClients;
    }
    
    
}
