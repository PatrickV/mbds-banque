/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Personne;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.mindrot.jbcrypt.BCrypt;
import session.GestionnaireDePersonne;
import tools.SessionUtils;

/**
 *
 * @author patrick
 */
@Named(value = "loginMB")
@SessionScoped
public class LoginMB implements Serializable {

    @EJB
    private GestionnaireDePersonne gestionnaireDePersonne;
    
    private String username;
    private String password;
    
    /**
     * Creates a new instance of LoginMB
     */
    public LoginMB() {
    }
    
    public String submit() {
        return validateUsernamePassword(username, password);
    }
    
    private String validateUsernamePassword(String username, String password) {
        Personne p = gestionnaireDePersonne.getPersonneUsername(username);
        
        if (p != null) {
            String hash = p.getPassword();
            if (BCrypt.checkpw(password, hash)) {
                String role = p.getDiscriminant();
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                session.setAttribute("username", username);
                session.setAttribute("_id", p.getId());
                session.setAttribute("role", role);
                
                if (role.equalsIgnoreCase("client")) {
                    return "/client/ClientListeComptes";
                } else if (role.equalsIgnoreCase("conseiller")) {
                    return "/conseiller/ConseillerListeClients";
                } else if (role.equalsIgnoreCase("administrateur")) {
                    return "/administrateur/AdministrateurListeConseillers";
                }
            }
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Login or Passowrd",
                            "Please enter correct login and password"));
            return "/Login";
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Login or Passowrd",
                            "Please enter correct login and password"));
            return "/Login";
        }
    }
    
    public String logout() {
        HttpSession session = SessionUtils.getSession();
        if (session != null) {
            session.invalidate();
        }
        return "/Login";
    }
    
    private String getType(Personne p) {
        String type = p.getClass().getName();
        
        return "";
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
}
