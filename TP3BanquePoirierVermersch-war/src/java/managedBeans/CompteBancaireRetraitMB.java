/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.CompteBancaire;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import session.GestionnaireDeCompteBancaire;

/**
 *
 * @author Manon
 */
@Named(value = "compteBancaireRetraitMB")
@SessionScoped
public class CompteBancaireRetraitMB implements Serializable{
    private int idCompte;
    private int montant;

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;

    /**
     * Creates a new instance of CompteBancaireRetraitMB
     */
    public List<CompteBancaire> getListeComptes(){
        return gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public int getCompte(){
        return idCompte;
    }
    
    public void setCompte(int idCompte){
        this.idCompte = idCompte;
    }
    
    public int getMontant(){
        return this.montant;
    }
    
    public void setMontant(int montant){
        this.montant = montant;
    }
    
    public void retirer(){
        gestionnaireDeCompteBancaire.retirer(getCompte(),getMontant());
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + "/ClientListeComptes.xhtml");
        } catch (IOException ex){
            Logger.getLogger(CompteBancaireDepotMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
