/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeClient;

/**
 *
 * @author Manon
 */
@Named(value = "clientDetails")
@SessionScoped
public class ClientDetailsMB implements Serializable {
    private int idClient;
    private Client client;
    
    @EJB
    private GestionnaireDeClient gestionnaireDeClient;
    
    /**
     * Creates a new instance of ClientDetailsMB
     */
    public ClientDetailsMB() {
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    
    public Client getDetails(){
        return client;
    }
    
    public String list(){
        System.out.println("###LIST###");
        return "ClientList";
    }
    
    public String update(){
        System.out.println("###UPDATE###");
        client = gestionnaireDeClient.updateClient(client);
        return "ClientList";
    }
    
    public String create(){
        client = new Client();
        System.out.println("###CREATE###");
        gestionnaireDeClient.creerClient(client);
        return "ClientList";
    }
    
    public void loadClient(){
        this.client = gestionnaireDeClient.getClient(idClient);
    }
}
