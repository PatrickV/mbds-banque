/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Personne;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDePersonne;

/**
 *
 * @author Manon
 */
@Named(value = "personneMB")
@SessionScoped
public class PersonneMB implements Serializable {

    @EJB
    private GestionnaireDePersonne gestionnaireDePersonne;
    /**
     * Creates a new instance of PersonneMB
     */
    public PersonneMB() {
    }
    
    public List<Personne> getPersonnes(){
        return gestionnaireDePersonne.getAllPersonnes();
    }
    
    public String showDetails (int id){
        return "PersonneDetails?idPersonne=" + id;
    }
    
    private List<Personne> personneList;
    
    @PostConstruct
    private void postConstruct(){
        this.personneList = gestionnaireDePersonne.getAllPersonnes();
    }
    
    public List<Personne> getPersonneList(){
        return this.personneList;
    }
}
