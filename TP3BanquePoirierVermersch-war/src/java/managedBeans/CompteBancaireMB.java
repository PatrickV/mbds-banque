/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.CompteBancaire;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeCompteBancaire;

/**
 *
 * @author patrick
 */
@Named(value = "compteBancaireMB")
@SessionScoped
public class CompteBancaireMB implements Serializable {

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    
    /**
     * Creates a new instance of CompteBancaireMB
     */
    public CompteBancaireMB() {
    }
    
    public List<CompteBancaire> getCompteBancaires () {
        return gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public String showDetails (int id) {
        return "ClientDetails?idCompte=" + id;
    } 
    
    private List<CompteBancaire> compteBancaireList;  
    
    @PostConstruct
    private void postConstruct () {
        this.compteBancaireList = gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public List<CompteBancaire> getCompteBancaireList () {
        return this.compteBancaireList;
    }
}
