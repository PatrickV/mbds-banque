/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.CompteBancaire;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import session.GestionnaireDeCompteBancaire;

/**
 *
 * @author Manon
 */
@Named(value = "compteBancaireTransfertMB")
@SessionScoped
public class CompteBancaireTransfertMB implements Serializable {
    
    private int idCompteDebit;
    private int idCompteCredit;
    private int montant;
    
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;

    /**
     * Creates a new instance of CompteBancaireTransfertMB
     */
    public CompteBancaireTransfertMB() {
    }
    
    public List<CompteBancaire> getListeComptesDebit(){
        return gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public List<CompteBancaire> getListeComptesCredit(){
        return gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public int getCompteDebit(){
        return this.idCompteDebit;
    }
    
    public void setCompteDebit(int idCompteDebit){
        this.idCompteDebit = idCompteDebit;
    }
    
    public int getCompteCredit(){
        return this.idCompteCredit;
    }
    
    public void setCompteCredit(int idCompteCredit){
        this.idCompteCredit = idCompteCredit;
    }
    
    public int getMontant(){
        return this.montant;
    }
    
    public void setMontant(int montant){
        this.montant = montant;
    }
    
    public void transferer(){
        gestionnaireDeCompteBancaire.transferer(getCompteDebit(), getCompteCredit(), getMontant());
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + "/ClientListeComptes.xhtml");
        } catch (IOException ex){
            Logger.getLogger(CompteBancaireTransfertMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
