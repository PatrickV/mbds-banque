/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Personne;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDePersonne;

/**
 *
 * @author Manon
 */
@Named(value = "personneDetails")
@SessionScoped
public class PersonneDetailsMB implements Serializable {
    private int idPersonne;
    private Personne personne;

    @EJB
    private GestionnaireDePersonne gestionnaireDePersonne;
    /**
     * Creates a new instance of PersonneDetailsMB
     */
    public PersonneDetailsMB() {
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    public Personne getDetails(){
        return personne;
    }
    
    public String list(){
        System.out.println("###LIST###");
        return "PersonneList";
    }
    
    public void loadPersonne(){
        this.personne = gestionnaireDePersonne.getPersonne(idPersonne);
    }
    
}
