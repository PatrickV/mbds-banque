/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import entities.Conseiller;
import javax.inject.Named;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import session.GestionnaireDeClient;
import session.GestionnaireDeConseiller;

/**
 *
 * @author patrick
 */
@Named(value = "conseillerAjouterClientMB")
@SessionScoped
public class ConseillerAjouterClientMB implements Serializable {
    
    private String nom;
    private String prenom;
    private String adresse;
    private String username;
    private String password;
    private int numeroClient;
    
    @EJB
    GestionnaireDeClient gestionnaireDeClient;
    @EJB
    GestionnaireDeConseiller gestionnaireDeConseiller;

    /**
     * Creates a new instance of ConseillerAjouterClientMB
     */
    public ConseillerAjouterClientMB() {
    }
    
    public String submit() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        
        int idConseiller = (Integer) session.getAttribute("_id");
        
        Conseiller conseiller = gestionnaireDeConseiller.getConseiller(idConseiller);
        Client client = new Client(nom, prenom, adresse, nom, password, numeroClient, conseiller);
        
        gestionnaireDeClient.creerClient(client);
        
        conseiller.ajouterUnClient(client);
        
        gestionnaireDeConseiller.updateConseiller(conseiller);
        
        return "ConseillerListeClients";
    }
    
    public String ajouterClient() {
        return "ConseillerAjouterClient";
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the numeroClient
     */
    public int getNumeroClient() {
        return numeroClient;
    }

    /**
     * @param numeroClient the numeroClient to set
     */
    public void setNumeroClient(int numeroClient) {
        this.numeroClient = numeroClient;
    }
    
}
