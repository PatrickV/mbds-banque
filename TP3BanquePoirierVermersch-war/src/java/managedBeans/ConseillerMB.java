/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Conseiller;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeConseiller;

/**
 *
 * @author Manon
 */
@Named(value = "conseillerMB")
@SessionScoped
public class ConseillerMB implements Serializable {

    @EJB
    private GestionnaireDeConseiller gestionnaireDeConseiller;
    /**
     * Creates a new instance of ConseillerMB
     */
    public ConseillerMB() {
    }
    
    public List<Conseiller> getConseillers(){
        return gestionnaireDeConseiller.getAllConseillers();
    }
    
    public String showDetails(int id){
        return "AdministrateurConseillerDetails?idConseiller=" + id;
    }
    
    private List<Conseiller> conseillerList;
    
    @PostConstruct
    private void postConstruct(){
        this.conseillerList = gestionnaireDeConseiller.getAllConseillers();
    }
    
    public List<Conseiller> getConseillerList(){
        return this.conseillerList;
    }
}
