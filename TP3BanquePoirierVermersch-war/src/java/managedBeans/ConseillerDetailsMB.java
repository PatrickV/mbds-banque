/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Conseiller;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import session.GestionnaireDeConseiller;

/**
 *
 * @author Manon
 */
@Named(value = "conseillerDetails")
@SessionScoped
public class ConseillerDetailsMB implements Serializable {
    private int idConseiller;
    private Conseiller conseiller;
    
    @EJB
    private GestionnaireDeConseiller gestionnaireDeConseiller;
    /**
     * Creates a new instance of ConseillerDetails
     */
    public ConseillerDetailsMB() {
    }
    
    public int getIdConseiller(){
        return idConseiller;
    }
    
    public void setIdConseiller(int idConseiller){
        this.idConseiller = idConseiller;
    }
    
    public Conseiller getDetails(){
        return conseiller;
    }
    
    public String list(){
        System.out.println("###LIST###");
        return "ConseillerList";
    }
    
    public void update(){
        System.out.println("###UPDATE###");
        conseiller = gestionnaireDeConseiller.updateConseiller(conseiller);
        FacesMessage message = new FacesMessage( "Succès de la mise à jour !" );
        FacesContext.getCurrentInstance().addMessage( null, message );
    }
    
    public void create(){
        gestionnaireDeConseiller.creerConseiller(conseiller);
    }
    
    public void loadConseiller(){
        this.conseiller = gestionnaireDeConseiller.getConseiller(idConseiller);
    }
    
    public void loadConseiller(int id){
        this.conseiller = gestionnaireDeConseiller.getConseiller(id);
    }
}

