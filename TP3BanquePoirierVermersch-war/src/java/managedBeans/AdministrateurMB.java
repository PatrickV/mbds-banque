/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Administrateur;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDAdministrateur;

/**
 *
 * @author Manon
 */
@Named(value = "administrateurMB")
@SessionScoped
public class AdministrateurMB implements Serializable {

    @EJB
    private GestionnaireDAdministrateur gestionnaireDAdministrateur;
    /**
     * Creates a new instance of AdministrateurMB
     */
    public AdministrateurMB() {
    }
    
    public List<Administrateur> getAdministrateurs(){
        return gestionnaireDAdministrateur.getAllAdministrateurs();
    }
    
    public String showDetails (int id){
        return "AdministrateurConseillerDetails?id=" + id;
    }
    
    private List<Administrateur> administrateurList;
    
    private void postConstruct (){
        this.administrateurList = gestionnaireDAdministrateur.getAllAdministrateurs();
    }
    
    public List<Administrateur> getAdministrateurList(){
        return this.administrateurList;
    }
}
