/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.CompteBancaire;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeCompteBancaire;

/**
 *
 * @author patrick
 */
@Named(value = "compteBancaireDetails")
@SessionScoped
public class CompteBancaireDetailsMB implements Serializable {
    private int idCompte;
    private CompteBancaire compteBancaire;
    
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    /**
     * Creates a new instance of CompteBancaireDetails
     * 
     */
    
    public int getIdCompte(){
        return idCompte;
    }
    
    public void setIdCompte(int idCompte){
        this.idCompte = idCompte;
    }
    
    public CompteBancaire getDetails() {
        return compteBancaire;
    }
    
    public String update(){
        System.out.println("###UPDATE###");
        compteBancaire = gestionnaireDeCompteBancaire.updateCompte(compteBancaire);
        return "ClientListeComptes";
    }
    
    public String list(){
        System.out.println("###LIST###");
        return "ClientListeComptes";
    }
    
    public void loadCompteBancaire(){
        this.compteBancaire = gestionnaireDeCompteBancaire.getCompte(idCompte);
    }
    
}
