/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Conseiller;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeConseiller;

/**
 *
 * @author patrick
 */
@Named(value = "administrateurAjouterConseillerMB")
@SessionScoped
public class AdministrateurAjouterConseillerMB implements Serializable {
    
    private String nom;
    private String prenom;
    private String adresse;
    private String username;
    private String password;
    private int numeroConseiller;
    
    @EJB
    GestionnaireDeConseiller gestionnaireDeConseiller;

    /**
     * Creates a new instance of AdministrateurAjouterClient
     */
    public AdministrateurAjouterConseillerMB() {
    }
    
    public String submit() {
        gestionnaireDeConseiller.creerConseiller(new Conseiller(nom, prenom, adresse, username, password, numeroConseiller));
        return "AdministrateurListeConseillers";
    }
    
    public String addConseiller() {
        return "AdministrateurAjouterConseiller";
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the numeroConseillier
     */
    public int getNumeroConseillier() {
        return numeroConseiller;
    }

    /**
     * @param numeroConseiller the numeroConseillier to set
     */
    public void setNumeroConseillier(int numeroConseiller) {
        this.numeroConseiller = numeroConseiller;
    }
    
}
