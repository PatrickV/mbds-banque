/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeClient;

/**
 *
 * @author patrick
 */
@Named(value = "conseillerClientDetailsMB")
@SessionScoped
public class ConseillerClientDetailsMB implements Serializable {
    
    private int idClient;
    private Client client;
    
    @EJB
    GestionnaireDeClient gestionnaireDeClient;

    /**
     * Creates a new instance of ConseillerClientDetailsMB
     */
    public ConseillerClientDetailsMB() {
    }
    
    public String showDetails(int id) {
        return "ConseillerDetailsClient?idClient=" + id;
    }
    
    public void loadClient(){
        this.client = gestionnaireDeClient.getClient(idClient);
    }
    
    public void loadClient(int id){
        this.client = gestionnaireDeClient.getClient(id);
    }
    
    public Client getDetails() {
        return this.client;
    }

    /**
     * @return the idClient
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     * @param idClient the idClient to set
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }
    
}
