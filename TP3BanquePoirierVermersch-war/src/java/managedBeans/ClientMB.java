/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import entities.Client;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeClient;

/**
 *
 * @author Manon
 */
@Named(value = "clientMB")
@SessionScoped
public class ClientMB implements Serializable {

    @EJB
    private GestionnaireDeClient gestionnaireDeClient;
    /**
     * Creates a new instance of ClientMB
     */
    public ClientMB() {
    }
    
    public List<Client> getClients(){
        return gestionnaireDeClient.getAllClients();
    }
    
    public String showDetails (int id){
        return "ClientDetails?idClient" + id;
    }
    
    private List<Client> clientList;
    
    @PostConstruct
    private void postConstruc(){
        this.clientList = gestionnaireDeClient.getAllClients();
    }
    
    public List<Client> getClientList(){
        return this.clientList;
    }
}
