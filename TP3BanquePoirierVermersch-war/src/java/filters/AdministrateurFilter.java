/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author patrick
 */
@WebFilter(filterName = "AdministrateurFilter", urlPatterns = {"/administrateur/*"})
public class AdministrateurFilter implements Filter {
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {    

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        if (session.getAttribute("role") != null) {
            String role = (String) session.getAttribute("role");
            if (role.equalsIgnoreCase("administrateur")) {
                try {
                    chain.doFilter(request, response);
                } catch (IOException | ServletException ex) {
                    Logger.getLogger(AdministrateurFilter.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    response.sendRedirect(request.getContextPath() + "/Login.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(AdministrateurFilter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            try {
                response.sendRedirect(request.getContextPath() + "/Login.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(AdministrateurFilter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
