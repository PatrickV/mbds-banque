# Projet Banque Poirier & Vermersch
## Informations :

 - Ayant eu de nombreux problème avec Glassfish, nous avons fait le choix de passer sous Payara pour notre serveur.  Pour ce faire, nous avons suivi cette procedure : [Payara](https://docs.payara.fish/documentation/ecosystem/netbeans-plugin/)

- Nous avons mit les filtres en place, ils se trouvent dans la partie web, package filtres. Pour ce connecter aux différents users, voici les logins est mots de passe : 
	- Client : login = client & mdp = password
	- Administrateur : login = admin & mdp = password
	- Conseiller : login = conseiller & mdp = password
(Si vous vous posez la question, les mots de passes sont différents ET encrypté dans la base de données, mais pour que ça soit plus simple à utiliser, nous avons mit le même mot de passe aux 3 users)

- Data source : jdbc/banque
- Configuration de la db :
	> <property name="serverName" value="localhost"/>
        <property name="portNumber" value="1527"/>
        <property name="databaseName" value="BanquePoirierVermersch"/>
        <property name="User" value="root"/>
        <property name="Password" value="toor"/>
        <property name="URL" value="jdbc:derby://localhost:1527/BanquePoirierVermersch"/>
        <property name="driverClass" value="org.apache.derby.jdbc.ClientDriver"/>


 